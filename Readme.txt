このファイルの
アカウントはAdmin
パスワードはadmin
です。

動作環境　filemaker ver16以上



最初にFirebaseでプロジェクトを作成し、プロジェクトIDを取得して下さい。
それをGlobalレイアウトのapp_nameフィールドに記入して下さい。

サンプルモデルのレコードはフィールドへの値の直接入力はできません。新規作成ボタンでカスタムダイアログが出ますのでその中に入力して下さい。入力が終わったタイミングでトリガーが発火してサーバーに送信されます。



